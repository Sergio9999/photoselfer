require 'rails_helper'

feature 'Authorization' do
  subject { page }

  describe 'signin page' do
    before { visit new_session_path }

    it { expect have_selector('h1', text: 'Sign in') }
    it { expect have_title('Sign in') }
  end

  describe 'signin' do
    before { visit new_session_path }

    describe 'with invaild information' do
      before { click_button 'Sign in' }

      it { expect has_selector?('title', text: 'Sign in') }
      it { expect have_selector('div.alert.alert-alert', text: 'invalid email or password') }
    end

    describe 'with valid information' do
      let(:user) { FactoryGirl.create(:user) }

      before do
        fill_in 'Email', with: user.email
        fill_in 'Password', with: user.password
        click_link 'Sign In'
      end
      it { expect has_selector?('title', text: user.name) }
      it { expect have_selector?('div.alert.alert-notice', text: 'Logged In') }
      it { expect have_selector?('h1', text: 'User Profile') }
      it { expect have_selector?('h4', text: user.name) }
      it { screenshot_and_save_page }
      it { expect have_selector?('h4', text: user.email) }
      it { expect have_link?(text: 'Sign Out', href: signout_path) }
      it { expect have_link?(text: 'My Profile') }
      it { expect(page).to_not have_link(text: 'Sign In') }

      describe 'after click link Sign Out' do
        before { visit new_session_path }

        it { expect have_selector('div.alert.alert-notice', text: 'Logged out. Good_bye!') }
        it { expect have_link(text: 'Sign In') }
      end
    end
  end
end
