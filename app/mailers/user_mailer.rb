class UserMailer < ApplicationMailer
  default from: 'SobolevSergey9999@gmail.com'

  def signup_confirmation(inviter)
    mail to: inviter, subject: 'Welcome'
  end
end
