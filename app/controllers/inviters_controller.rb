class InvitersController < ApplicationController
  def create
    inviter = params[:email]
    if inviter
      UserMailer.signup_confirmation(inviter).deliver_now
      redirect_to inviter
    end
  end
end
