class OmniauthController < ApplicationController
before_filter :current_resources

  def create
    if @user
      @account[user: @user]
    else
      @user = @account.create_user(vk_params)
    end
    @account.save
    session[:user_id] = @user.id
    redirect_to @user
  end

  private

  def vk_params
    { name: auth_hash[:info][:name],
      email: auth_hash[:info][:email]
    }
  end

  def current_resources
    @account = Account.find_or_create_by(uid: auth_hash[:uid],
                                         provider: auth_hash[:provider],
                                         email: auth_hash[:info][:email])
    @user = User.find_by(email: auth_hash[:info][:email])
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
