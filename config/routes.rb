Rails.application.routes.draw do
  root 'static_pages#index'
  resources :users, only: :show
  resources :inviters, only: :create
  delete :signout, to: 'sessions#destroy'

  get 'auth/vkontakte/callback', to: 'omniauth#create'
end
